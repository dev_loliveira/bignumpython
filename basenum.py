
class BaseNum(object):

    def __init__(self, n):
        self.n = n if type(n) == str else str(n)

    def scientific_notation(self, t_int=2, t_decimals=3):
        int_part = self.n[:t_int]
        decimals = self.n[t_int:]

        def _round(decimals, t_decimals):
            decimals = [d for d in decimals]
            index = len(decimals) - 1
            comma_mov = t_decimals if t_decimals <= len(decimals) else len(decimals)
            while index > t_decimals:
                current_d = int(decimals[index])
                prev_d = int(decimals[index-1])
                if current_d > 5:
                    if prev_d < 9:
                        decimals[index-1] = str(prev_d + 1)

                comma_mov += 1
                index -= 1
            return comma_mov, ''.join(decimals[:t_decimals])

        exponent, rounded_decimals = _round(decimals, t_decimals)

        if rounded_decimals:
            result = '%s.%se+%s' % (int_part, rounded_decimals, exponent)
        else:
            result = '%s' % (int_part)
        return result

    def extract_digits(self, n, digits=-1):
        n = [int(d) for d in str(n)]
        if len(n) < digits:
            sub_array = [0 for i in xrange(digits-len(n))]
            [sub_array.append(i) for i in n]
            n = sub_array
        return n

    def balance_digits(self, n1, n2):
        n1 = self.get_correct_object(n1)
        n2 = self.get_correct_object(n2)
        return len(n1) if len(n1) > len(n2) else len(n2)

    def get_correct_object(self, obj):
        if type(obj) in (list, str):
            return obj
        elif type(obj) == int:
            return BaseNum(obj)
        else:
            return obj.n

    def __eq__(self, n):
        n_digits = self.balance_digits(self.n, n)
        n1, n2 = self.extract_digits(self.n, n_digits), self.extract_digits(n, n_digits)
        return n1 == n2

    def __ne__(self, n):
        n_digits = self.balance_digits(self.n, n)
        n1, n2 = self.extract_digits(self.n, n_digits), self.extract_digits(n, n_digits)
        return n1 != n2

    def __lt__(self, n):
        n_digits = self.balance_digits(self.n, n)
        n1, n2 = self.extract_digits(self.n, n_digits), self.extract_digits(n, n_digits)
        for i in xrange(n_digits):
            if n1[i] < n2[i]:
                return True
            elif n1[i] > n2[i]:
                return False
        return False

    def __gt__(self, n):
        n_digits = self.balance_digits(self.n, n)
        n1, n2 = self.extract_digits(self.n, n_digits), self.extract_digits(n, n_digits)
        for i in xrange(n_digits):
            if n1[i] < n2[i]:
                return False
            elif n1[i] > n2[i]:
                return True
        return False

    def __ge__(self, n):
        return self.__gt__(n) or self.__eq__(n)

    def __le__(self, n):
        return self.__eq__(n) or self.__lt__(n)

    def __str__(self):
        return str(self.n)

    def toArray(self):
        return [int(n) for n in str(self)]
