from basenum import BaseNum
from bignum_add import BigNumADD
from bignum_mul import BigNumMUL
from bignum_sub import BigNumSub


class BigNum(BaseNum):

    def __init__(self, n):
        super(BigNum, self).__init__(n)

    def __add__(self, n):
        add_handler = BigNumADD(0)
        tmp = add_handler.add(str(self), self.get_correct_object(n))
        return BigNum(int(''.join([str(d) for d in tmp])))

    def __mul__(self, obj):
        mul_handler = BigNumMUL(1)
        obj = obj.n if type(obj) == BigNum else obj
        tmp = mul_handler.multiply(self.n, obj)
        return BigNum(int(''.join([str(d) for d in tmp])))

    def __sub__(self, obj):
        handler = BigNumSub(self.n)
        tmp = handler.sub(self.get_correct_object(obj))
        return BigNum(''.join([str(d) for d in tmp]))

    def pow(self, i):
        if i > 0:
            tmp = BigNum(self.n)
            for n in range(1, i):
                tmp = tmp*self
            return tmp
        return BigNum(1)


class Primality(BigNum):

    def is_prime(self):
        checks = (
            self.check_division_by_2,
            self.check_division_by_3,
            self.check_division_by_5,
            self.check_division_by_7,
            self.check_division_by_8,
            self.check_division_by_11,
        )

        for check in checks:
            if check():
                return False
        return True

    def check_division_by_2(self):
        return self.n[-1] in ('0', '2', '4', '6', '8')

    def check_division_by_3(self):
        sum_digits = sum([int(d) for d in self.n])
        return (sum_digits % 3) == 0

    def check_division_by_5(self):
        return self.n[-1] in ('0', '5')

    def check_division_by_7(term):
        raise NotImplementedError

    def check_division_by_8(term):
        digits = [c for c in term.n]
        n_digits = len(digits)
        if n_digits > 3:
            last_digits = [
                digits[n_digits-1],
                digits[n_digits-2],
                digits[n_digits-3]
            ]
            value = int(''.join([d for d in last_digits]))
        else:
            value = int(''.join(digits))
        return value % 8 == 0

    def check_division_by_9(self):
        return (sum_digits % 9) == 0

    def check_division_by_11(term):
        digits = term.toArray()
        odd_terms = [digits[i] for i in range(0, len(digits)) if (i+1) % 2 != 0]
        even_terms = [digits[i] for i in range(0, len(digits)) if (i+1) % 2 == 0]
        result = sum(odd_terms) - sum(even_terms)
        return result == 0 or (result % 11) == 0
