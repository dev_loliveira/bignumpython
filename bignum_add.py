
from basenum import BaseNum


class BigNumADD(BaseNum):

    def __init__(self, n):
        super(BigNumADD, self).__init__(n)

    def add(self, n1, n2):
        n_digits = self.balance_digits(n1, n2)
        n1, n2 = self.extract_digits(n1, n_digits), self.extract_digits(n2, n_digits)
        tmp = []
        asc, val = 0, 0
        i = n_digits-1
        while i >= 0:
            val = n1[i] + n2[i] + asc
            if val >= 10:
                diff = val%10
                val = diff
                asc = 1
            else:
                asc = 0
            tmp.append(val)
            i -= 1
        if asc > 0:
            tmp.append(asc)
        tmp.reverse()
        return tmp

