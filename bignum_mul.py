from basenum import BaseNum
from bignum import BigNum
from bignum_add import BigNumADD


class BigNumMUL(BaseNum):
    BASE = 10

    def multiply(self, n1, n2):
        if n1 == 0 or n2 == 0:
            return BigNum(0)

        n1 = n1 if type(n1) is BigNum else BigNum(n1)
        n2 = n2 if type(n2) is BigNum else BigNum(n2)

        digits_n1 = n1.toArray()
        digits_n2 = n2.toArray()

        index = len(digits_n2) - 1
        digit_inc = 0
        tmp_numbers = list()
        for j in range(len(digits_n2)-1, -1, -1):
            digit_n2 = digits_n2[j]
            borrow = 0
            digits = [0 for i in range(0, digit_inc)]
            for i in range(len(digits_n1)-1, -1, -1):
                digit_n1 = digits_n1[i]

                prod = (digit_n1 * digit_n2) + borrow
                if prod >= 10:
                    borrow = prod / 10
                    digit_r = prod % 10
                else:
                    borrow = 0
                    digit_r = prod

                digits.append(digit_r)

            if borrow > 0:
                digits.append(borrow)

            digits.reverse()
            digits = [str(d) for d in digits]

            tmp_number = ''.join(digits)
            tmp_numbers.append(tmp_number)
            digit_inc += 1

        result = BigNum(tmp_numbers.pop())
        for number in tmp_numbers:
            n2 = BigNum(number)
            result = result + n2

        return result
