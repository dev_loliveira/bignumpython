
from basenum import BaseNum
from copy import copy


class BigNumSub(BaseNum):

    def sub(self, n):
        def remove_leading_zeroes(vector):
            tmp = []
            for d in vector:
                if (d == 0 and len(tmp) == 0): continue
                tmp.append(d)

            if len(tmp) == 0:
                tmp = [0]
            return tmp

        n_digits = self.balance_digits(self.n, n)
        n1, n2 = self.extract_digits(self.n, n_digits), self.extract_digits(n, n_digits)
        negative = False

        if n1 < n2:
            tmp = copy(n1)
            n1 = copy(n2)
            n2 = copy(tmp)
            negative = True

        index = n_digits-1
        d_r = -1
        borrow = 0
        vector = []
        while index >= 0:
            d_1, d_2 = (n1[index]-borrow), n2[index]
            if d_1 - d_2 < 0:
                borrow = 1
                d_r = (d_1 - d_2) + 10
            else:
                d_r = d_1 - d_2
                borrow = 0

            vector.append(d_r)
            index -= 1

        vector.reverse()
        vector = remove_leading_zeroes(vector)
        if negative:
            vector.insert(0, '-')

        return (vector)
