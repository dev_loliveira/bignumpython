import os
import sys
from unittest import main

# Adding the project's root directory to the sys path so that the modules can
# be found
PROJECT_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, PROJECT_ROOT)


def valid_import(f_name):
    if f_name.endswith('.py') and f_name not in ('__init__.py','__main__.py'):
        return True
    return False

def generate_imports(dir_name='.'):
    imports = []
    for f_name in os.listdir(dir_name):
        if not valid_import(f_name):
            continue
        cmd = 'from tests.%s import *' % (f_name.replace('.py', ''))
        imports.append(cmd)
    return imports

if __name__ == '__main__':
    imports = generate_imports('tests')
    for cmd in imports:
        exec(cmd)
    main()
