from unittest import TestCase
from basenum import BaseNum

class BaseNumTest(TestCase):

    def test_scientific_notation_should_return_correct_representation_of_the_number(self):
        inputs = [
            ('123', BaseNum('123').scientific_notation(t_int=3, t_decimals=2)),
            ('12.3e+1', BaseNum('123').scientific_notation(t_int=2, t_decimals=2)),
            ('1.23e+2', BaseNum('123').scientific_notation(t_int=1, t_decimals=2))
        ]

        for expected, returned in inputs:
            self.assertEquals(expected, returned)

    def test_get_correct_object_should_basenum_instance_when_argument_is_int(self):
        n1 = BaseNum(1)
        self.assertEqual(BaseNum, type(n1.get_correct_object(1)))
