from unittest import TestCase
from bignum import BigNum

class BigNumOperatorEQ(TestCase):

    def test_class_should_have_eq_attribute(self):
        self.assertTrue(hasattr(BigNum(0), '__eq__'), 'Class should have __eq__ method implemented.')

    def test_operator_eq_should_return_true_if_numbers_are_equal(self):
        for i in xrange(0, 100):
            n1 = BigNum(i)
            n2 = BigNum(i)
            self.assertTrue(n1 == n2)

    def test_operator_eq_should_return_false_if_numbers_are_equal(self):
        for i in xrange(0, 100):
            n1 = BigNum(i)
            n2 = BigNum(i+1)
            self.assertFalse(n1 == n2)

