from unittest import TestCase
from bignum import BigNum

class BigNumOperatorGE(TestCase):

    def test_class_should_have_ge_attribute(self):
        self.assertTrue(hasattr(BigNum(0), '__ge__'), 'Class should have __ge__ method implemented.')

    def test_operator_ge_should_work_with_equal_number_of_digits(self):
        for i in xrange(0, 100):
            n1 = BigNum(i+1)
            n2 = BigNum(i)
            self.assertTrue(n1 >= n2)

    def test_operator_ge_should_work_with_different_number_of_digits(self):
        for i in xrange(1, 100):
            n1 = BigNum(i*10)
            n2 = BigNum(i)
            self.assertTrue(n1 >= n2)

    def test_operator_ge_should_return_true_if_number_is_equal_to_argument(self):
        n1 = BigNum(0)
        n2 = BigNum(0)
        self.assertTrue(n1 >= n2)

    def test_operator_ge_should_return_false_if_number_is_lower_than_argument(self):
        for i in xrange(0, 100):
            n1 = BigNum(i)
            n2 = BigNum(i+1)
            self.assertFalse(n1 >= n2)

    def test_operator_ge_should_return_false_if_number_is_lower_than_argument_and_has_more_digits(self):
        for i in xrange(1, 100):
            n1 = BigNum(i)
            n2 = BigNum(i*10)
            self.assertFalse(n1 >= n2)
