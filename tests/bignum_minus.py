
from unittest import TestCase
from bignum import BigNum


class BigNumSubTest(TestCase):

    def test_sub_should_work_properly_with_two_objects_of_bignum(self):
        for i in range(1, 100):
            for j in range(100, 1, -1):
                n1 = BigNum(i)
                n2 = BigNum(j)
                expected = str(j - i)
                returned = str(n2 - n1)
                self.assertEquals(expected, returned, '%d - %d = %s [%s]' % (j, i, expected, returned))

    def test_sub_should_work_properly_when_argument_is_of_int_type(self):
        for i in range(1, 100):
            for j in range(100, 1, -1):
                n = BigNum(j)
                expected = str(j - i)
                returned = str(n - i)
                self.assertEquals(expected, returned, '%d - %d = %s [%s]' % (j, i, expected, returned))
