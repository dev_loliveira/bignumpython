from unittest import TestCase
from bignum import BigNum


class BigNumMultiplicationTest(TestCase):

    def test_multiplication_with_one_digit_should_work_properly(self):
        for i in xrange(9):
            for j in xrange(9):
                n1 = BigNum(i)
                self.assertEquals(str(n1*j), str(i*j), 'Multiplication error. %d x %d = %d [%s]' % (i, j, i*j, str(n1*j)))

    def test_multiplication_with_different_number_of_digits_should_work_properly(self):
        for i in xrange(99):
            for j in xrange(9):
                n1 = BigNum(i)
                self.assertEquals(str(n1*j), str(i*j), 'Multiplication error. %d x %d = %d [%s]' % (i, j, i*j, str(n1*j)))

    def test_multiplicand_and_multiplier_should_be_calculated_automatically(self):
        for i in xrange(9):
            for j in xrange(99):
                n1 = BigNum(i)
                self.assertEquals(str(n1*j), str(i*j), 'Multiplication error. %d x %d = %d [%s]' % (i, j, i*j, str(n1*j)))

    def test_multiplication_with_big_numbers_should_work_properly(self):
        for i in range(900, 1001):
            for j in range(400, 500):
                n1 = BigNum(i)
                self.assertEquals(str(n1*j), str(i*j), 'Multiplication error. %d x %d = %d [%s]' % (i, j, i*j, str(n1*j)))

    def test_multiplication_with_large_string_numbers_should_return_the_expected_result(self):
        n1 = BigNum('2000000000000000000000000000000')
        n2 = BigNum('4000000000000000000000000000000')
        expected = '8' + ''.join(['0' for i in range(60)])
        self.assertEquals(n1*n2, expected)

    def test_should_be_able_to_multiply_two_objects_of_bignum(self):
        for i in range(100):
            n1 = BigNum(i)
            n2 = BigNum(i)
            expected = i*i
            returned = n1*n2
            self.assertEquals(str(expected), str(returned), '%dx%d = %d [%s]' % (i, i, expected, returned))

    def test_should_be_to_multiply_against_a_integer(self):
        for i in range(100):
            n = BigNum(i)
            expected = i*2
            returned = n*2
            self.assertEquals(str(expected), str(returned), '%d x 2 = %d [%s]' % (i, expected, returned))

    def test_should_be_able_to_multiply_against_a_string(self):
        for i in range(100):
            n = BigNum(i)
            expected = i*2
            returned = n*'2'
            self.assertEquals(str(expected), str(returned), '%d x 2 = %d [%s]' % (i, expected, returned))

    def test_should_respect_the_associative_property(self):
        a = 123456789
        b = 987654321
        bigA = BigNum(a)
        bigB = BigNum(b)

        self.assertEquals(str(bigA*bigB), str(bigB*bigA))
