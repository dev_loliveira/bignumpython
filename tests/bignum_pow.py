import math
from unittest import TestCase
from bignum import BigNum


class BigNumPow(TestCase):

    def test_pow_should_return_the_correct_value(self):

        for i in xrange(1, 100):
            n = BigNum(2)
            expected = int(math.pow(2, i))
            returned = n.pow(i)
            self.assertEquals(str(expected), str(returned), '2^%d = %d [%s]' % (i, expected, returned))

    def test_pow_zero_should_return_one(self):
        for i in range(1, 100):
            n = BigNum(i)
            expected = int(math.pow(i, 0))
            returned = n.pow(0)
            self.assertEquals(str(expected), str(returned), '2^%d = %d [%s]' % (0, expected, returned))

    def test_pow_one_should_return_the_same_number(self):
        for i in range(1, 100):
            n = BigNum(i)
            expected = str(n)
            returned = n.pow(1)
            self.assertEquals(str(expected), str(returned), '%d^1 = %s [%s]'%(i, expected, returned))
