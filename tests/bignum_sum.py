
from unittest import TestCase
from bignum import BigNum


class BigNumSumTest(TestCase):

    def test_sum_with_equal_number_of_digits_should_work_properly(self):
        for i in xrange(0, 100):
            for j in xrange(0, 100):
                n1 = BigNum(i)
                n2 = BigNum(j)
                self.assertEquals(str(n1+n2), str(i+j))

    def test_sum_with_different_number_of_digits_should_work_properly(self):
        for i in xrange(0, 100):
            for j in xrange(1000, 2000):
                n1 = BigNum(i)
                n2 = BigNum(j)
                self.assertEquals(str(n1+n2), str(i+j))

    def test_sum_should_work_with_integers(self):
        for i in xrange(0, 100):
            n = BigNum(i)
            expected = i+1
            returned = n+1
            self.assertEquals(str(expected), str(returned), '%d+1 = %d [%s]' % (i, expected, returned))

    def test_sum_should_work_with_strings(self):
        for i in xrange(0, 100):
            n = BigNum(i)
            expected = i+1
            returned = n+'1'
            self.assertEquals(str(expected), str(returned), '%d+1 = %d [%s]' % (i, expected, returned))
